﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Interfaces
{
    interface IObjectService
    {
        Task<Models.ObjectModel> CreateAsync(Models.ObjectModel obj);
        Task<Models.ObjectModel> UpdateAsync(int objectId, Models.ObjectModel obj);
    }
}
