﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Interfaces
{
    interface IConfigurationService
    {
        Task<Models.ConfigurationModel> GetConfigurationAsync(string appId, string secure, string engineName);
    }
}
