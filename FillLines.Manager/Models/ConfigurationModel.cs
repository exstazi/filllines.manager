﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Models
{
    class ConfigurationModel
    {
        public Guid AppId { get; set; }
        public string Secure { get; set; }
        public string EngineName { get; set; }

        public List<NodeEnum> Nodes { get; set; }
        public List<EdgeEnum> Edges { get; set; }
        public List<ObjectEnum> Objects { get; set; }
    }
}
