﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Models
{
    class EdgeModel
    {
        public int Id { get; set; }
        public int ObjectId { get; set; }
        public Guid ProjectId { get; set; }
        public int FirstNodeId { get; set; }
        public int SecondNodeId { get; set; }
        public EdgeEnum Type { get; set; }
    }
}
