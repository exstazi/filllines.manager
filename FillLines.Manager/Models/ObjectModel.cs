﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Models
{
    class ObjectModel
    {
        public int Id { get; set; }
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public string Tagname { get; set; }
        public string Construiction { get; set; }
        public int Version { get; set; }
        public ObjectEnum Type { get; set; }
    }
}
