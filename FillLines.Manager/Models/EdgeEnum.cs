﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager.Models
{
    enum EdgeEnum
    {
        Simple,
        NonReturnValve,
        Valve,
        Stub,
        Pump
    }
}
