﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Manager
{
    public class Manager
    {
        private const string DEFAULT_END_POINT = "http://localhost:8558";

        static void Init(string appId, string secure)
        {
            Init(appId, secure, null, DEFAULT_END_POINT);
        }

        static void Init(string appId, string secure, string engineName)
        {
            Init(appId, secure, engineName, DEFAULT_END_POINT);
        }

        static void Init(string appId, string secure, string engineName, string endPoint)
        {

        }

        static void RegisterObject(string tagname, Models.ObjectEnum type, string name, string construction, string version)
        {

        }
    }
}
