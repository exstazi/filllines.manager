## Зависимости проекта
- FillLines.Core (https://bitbucket.org/exstazi/filllines.core)
- EmbedIO (https://github.com/unosquare/embedio)
- Swan.Lite (это зависимость от EmbedIO)
- Newtonsoft.Json (https://www.newtonsoft.com/json)
- System.ValueTuple (это зависимость от EmbedIO)
- .net 4.7.2 **Необходимо установить как минимум 4.7.2 (http://go.microsoft.com/fwlink/?linkid=863265)**

Данные зависимости можно получить после сборки проекта (в папке bin/Release)

> **Установка в ArchestraIde**
>  Выбираем в меню Galaxy > Import > Script Function Library ...
>  Открываем все указанные выше dll
>  **Бага!** Во время теста получилось так, что при деплои не все файлы перенеслись в платформу. Для решения этой проблемы, необходимо вручную переместить зависимости проекта на машину и положить их:
>  C:\Program Files (x86)\ArchestrA\Framework\Bin

# Настройка FillLines
Шаг 1. Инициализация библиотеки FillLines
Статический класс **Manager** в пространстве имен **FillLines** предоставляет методы инициализации приложения:

`Init(string appId, string appSecure)`

`Init(string appId, string appSecure, string engineName)`

`Init(SettingModel setting)`